# Fix music collection names [![GPL v3](/gplv3-88x31.png)](LICENSE)

This is a bash script to help keep your music collection's file names clean.

Some programs don't mind putting spaces and other messy characters in file names, which can be annoying if you are doing terminal work with your files, or which cause problems with badly written programs. This program removes them.

This little program requires bash, sed and some other programs you'll typically find pre-installed on Linux distributions.

## Example of effect

Before:

	Music
	├── Sigur Rós
	│   └── ( )
	│       ├── 01 - Untitled #1 [Vaka].ogg
	│       ├── 02 - Untitled #2 [Fyrsta].ogg
	│       ├── 03 - Untitled #3 [Samskeyti].ogg
	│       ├── 04 - Untitled #4 [Njósnavélin].ogg
	│       ├── 05 - Untitled #5 [Álafoss].ogg
	│       ├── 06 - Untitled #6 [E-Bow].ogg
	│       ├── 07 - Untitled #7 [Dauðalagið].ogg
	│       └── 08 - Untitled #8 [Popplagið].ogg
	└── Simon & Garfunkel
	    └── Bridge over Troubled Water
	        ├── 1 - Bridge over Troubled Water.ogg
	        ├── 2 - El Condor Pasa (If I Could).ogg
	        ├── 3 - Cecilia.ogg
	        ├── 4 - Keep the Customer Satisfied.ogg
	        └── 5 - So Long, Frank Lloyd Wright.ogg

After:

	Music
	├── Sigur_Rós
	│   └── Untitled
	│       ├── 01_-_Untitled_1_Vaka.ogg
	│       ├── 02_-_Untitled_2_Fyrsta.ogg
	│       ├── 03_-_Untitled_3_Samskeyti.ogg
	│       ├── 04_-_Untitled_4_Njósnavélin.ogg
	│       ├── 05_-_Untitled_5_Álafoss.ogg
	│       ├── 06_-_Untitled_6_E-Bow.ogg
	│       ├── 07_-_Untitled_7_Dauðalagið.ogg
	│       └── 08_-_Untitled_8_Popplagið.ogg
	└── Simon_and_Garfunkel
	    └── Bridge_over_Troubled_Water
	        ├── 1_-_Bridge_over_Troubled_Water.ogg
	        ├── 2_-_El_Condor_Pasa_If_I_Could.ogg
	        ├── 3_-_Cecilia.ogg
	        ├── 4_-_Keep_the_Customer_Satisfied.ogg
	        └── 5_-_So_Long_Frank_Lloyd_Wright.ogg

## Usage

Download the bash script, navigate to its folder in a terminal and execute it to clean `$HOME/Music`:

	./fix_names.sh

You can configure the default location in the script itself.

You can also pass the folder you want to clean as an argument, e.g.:

	./fix_names.sh /mnt/storage/music

## Copyright

Written in 2016 by Midgard

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.
