#!/bin/bash

# Fix music collection names
# Written in 2016 by Midgard

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

### CONFIG #####################################################################
to_remove=( \' '"' '(' ')' '*' '[' ']' '{' '}' '\' '!' '?' ) # Characters to remove
to_replace=( ' ' ',' '#' ) # Characters to replace with an underscore
default_music_location="$HOME/Music"
################################################################################

# mkbash's boilerplate

cmd="$(basename "$0")"

show_syntax() {
	echo "Syntax: $cmd [OPTIONS] [folder]" >&2
	echo >&2
	echo "  Options:" >&2
	echo "   -h  Show this help text and exit" >&2
}
syntax_error() {
	show_syntax
	exit 1
}

while getopts "h" opt; do
	case "$opt" in
		h) show_syntax; exit; ;;
		\?) syntax_error; ;;
	esac
done
shift $((OPTIND-1))

if [ $# -gt 1 ]; then
	syntax_error
fi



if [ -n "$1" ]; then
	MUSICCOLLECTIONLOC="$1"
elif [ -z "$MUSICCOLLECTIONLOC" ]; then
	MUSICCOLLECTIONLOC="$default_music_location"
fi

# Characters that need to be escaped in the regexp
escape=( '\' '[' ']' '.' '*' )
function make_regexp_part() {
	# Make a regexp part matching any of the strings passed as argument

	# In the first iteration it is simply the string itself. Later, an escaped pipe character (regexp OR) is prepended to each character
	format='%s'
	first=true

	while (( "$#" )); do
		char="$1"
		
		if [ "$char" = "/" ]; then
			echo "Trying to remove forward slashes (/) from a path may not be the best idea. Check the config." >&2
			exit 1
		fi
		
		# Escape characters that need to be escaped to include them in the regexp
		for to_escape_char in "${escape[@]}"; do
			char="${char//"$to_escape_char"/\\$to_escape_char}"
		done
		printf "$format" "$char"

		if [ $first = true ]; then
			format='\|%s'
			first=false
		fi

		shift
	done
}

to_remove_regexp_part="$( make_regexp_part "${to_remove[@]}" )"
to_replace_regexp_part="$( make_regexp_part "${to_replace[@]}" )"

moved=0
not_moved=0
total=0
modulus=0

echo -n "Processing ... "

# Reading lines of music collection (redirected to stdin of the while loop at the end of it. Don't use a pipe, because that puts the loop itself in a subshell which has some annoying side effects, like we couldn't access $moved and $not_moved outside the loop)
while read oldpath; do

	(( total ++ ))

	(( modulus ++ ))
	if [ $modulus -eq 100 ]; then
		echo -ne "\rProcessing ... done $total"
		modulus=0
	fi

	dirname="$(dirname "$oldpath")"
	oldname="$(basename "$oldpath")"

	newname="$(printf '%s' "$oldname" | sed "
		# Remove blacklisted characters
		s/$to_remove_regexp_part//g
		# Replace characters with underscore
		s/$to_replace_regexp_part/_/g
		# Replace ampersands and plus signs with '_and_'
		s/[&+]/_and_/g
		# Replace more than one underscore with one
		s/___*/_/g
		# Now remove underscores at beginning or end of filename
		s/^__*//; s/__*$//
	")"

	# If the remaining filename is empty, the file is "untitled"
	if [ -z "$newname" ]; then
		newname="Untitled"
	fi

	if [ "$oldname" = "$newname" ]; then
		(( not_moved ++ ))
	else
		echo Moving \""$dirname/$oldname"\" to \""$dirname/$newname"\"
		mv -Tn "$dirname/$oldname" "$dirname/$newname"
		if [ $? -eq 0 ]; then
			(( moved ++ ))
		else
			# Print newline to avoid error message being overwritten
			echo
		fi
	fi

# End of the while loop. Redirect the names of every file in the music collection to its stdin. -depth to rename files *before* their ancestors
done < <( find "$MUSICCOLLECTIONLOC" -depth -mindepth 1 )

echo -e "\rProcessing ... done $total"
echo "---"
echo "Renamed files and folders: $moved"
echo "Already good name: $not_moved"
echo "Errors: $(( total - moved - not_moved ))"

[ "$moved" -ne 0 ] && which mpd >/dev/null 2>&1 && echo "---" && echo "Don't forget to update your MPD database before trying to play music!"
